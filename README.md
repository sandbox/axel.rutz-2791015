Date Series
===========

This module implements multiple / recurring date handling by basically syncing

* a (single valued) _date_ field of an _event_ (an entity of arbitrary type).
* a (multiple valued) _dates_ field of the linked _date series_ (a special entity type).

Having multiple events instead of a single event with a multivalued date field solves some well-known problems:

* Any event can have different field values (e.g. description)
* Search api can index any event by itself

Importer support
----------------

Beyond the basic use case of a human user adding, changing and deleting date events,
this module also supports the use case of a importer (like feeds) that imports entities
with multiple date events (via rrule) and does some magick to multiply these events.

Installation and configuration
------------------------------

* Enable the _dateseries_ module
* Look into the _dateseries_example_ module and grok how the fields are configured.
* Change the example node type or configure your own with the provided fields.
  (Note: The field behaviors are (currently) hardcoded to the field names.)

Date event field configuration
-----------------------------

* dateseries_date (required): Date range field of an event
* dateseries_series (required): Entityreference to the series
* dateseries_add (optional): Add-multiple-events field
* dateseries_change (optional): Helper for imports

If you use the optional fields, you probably want to hide them e.g. via ds_forms.

Technical notes
===============

Manual workflow
---------------

* A new _date event_ is created
    * Its series is created and linked
    * The dateseries_dates field of the series is populated
* The add-dates field of a date event (on a bulk-clone edit tab) is filled
    * For any date of the dateseries_add field, a new date event with that dateseries_date value is created (as a clone of the original event)
    * The dateseries_add field is emptied then
* The date of a date event is changed (manually or via VBO)
    * The dateseries_dates field of the series is updated accordingly
* A date event is deleted (manually or via VBO)
    * Its dateseries_dates field of the series is adjusted accordingly
    * If it was the last event of the series, the series entity is deleted too.

Series importer workflow
-----------------

This is triggered by using the dateseries_change field. The delete use case is triggered by
a global flag that is set by feeds hooks.

* A new date event with the dateseries_change field is created
    * After the series is created and linked, its dateseries_dates field is set, and the date event
      is cloned multiple times if needed.
* A date event with the dateseries_change field is updated
    * For added dates, the date event is cloned and its dateseries_date field set
    * For removed dates, the date corresponding date events are deleted
* A date event is deleted while in import mode
    * The series and all its date events are deleted

Note that this workflow assumes that all series events are beyond the date field identical
and a mix of importer and manual workflow does not make sense.

Inner Workings
--------------

How the module implements it:

* Before an event is saved
    * If dateseries_dates import field is nonempty (i.e. contains at least one date),
        * it is copied to the dateseries_dates field and then emptied
          (and the changed flag set if it changed)
        * the current series events are fetched
        * the first date (which always exits) is assigned to the current event
        * dates are assigned to current events
        * more events are created if needed (with a do-not-process mark)
        * or existing events are deleted if needed (with a do-not-process mark)
    * If dateseries_add field is nonempty,
        * it is emptied
        * the corresponding date events are created and saved (with a do-not-process mark)
        * the series dateseries_dates field is updated (and the changed flag set)
    * Update dates of series
* After an event is deleted
    * If last event of a series, delete the series
    * If in import mode, delete the series and its other events
    * Else adjust series:dates

Implementation notes
--------------------

* For the current functionality, the series dateseries_dates field is not needed.
It is here for the future use case where a recurring rule is set and then can be changed.
Note that we then need an algorithm to map old and new dates.


Ideas for future development
----------------------------

* Make our field behaviors configurable instead of name-hardcoded. (D8; leverage forthcoming field behavior API)
* Allow any entity type as series (autocreate required fiels then)
* Make the series dates field optional.
* Add a updater for rrule fields that updates the rrule when events are added / deleted / edited manually.
* Add an action like "Detach event from series" (easy if the seres ref is made editable.)
* Add a date event magic field for "edit current / future / all events" (currently dropped in favor of VBO)
