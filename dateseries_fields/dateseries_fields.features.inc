<?php
/**
 * @file
 * dateseries_fields.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function dateseries_fields_eck_bundle_info() {
  $items = array(
    'dateseries_dateseries' => array(
      'machine_name' => 'dateseries_dateseries',
      'entity_type' => 'dateseries',
      'name' => 'dateseries',
      'label' => 'Date Series',
      'config' => array(),
    ),
    'dateseries_dummy' => array(
      'machine_name' => 'dateseries_dummy',
      'entity_type' => 'dateseries',
      'name' => 'dummy',
      'label' => 'Dummy',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function dateseries_fields_eck_entity_type_info() {
  $items = array(
    'dateseries' => array(
      'name' => 'dateseries',
      'label' => 'Date Series',
      'properties' => array(),
    ),
  );
  return $items;
}
