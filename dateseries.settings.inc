<?php
/**
 * @file dateseries.settings.inc
 */

function dateseries_settings_form($form, &$form_state) {
  $form['dateseries_log_level'] = [
    '#type' => 'radios',
    '#options' => \Drupal\dateseries\Tools\Logger::logLevelOptions(),
    '#title' => t('Log level'),
    '#default_value' => variable_get('dateseries_log_level'),
    '#description' => t('What will be logged to the watchdog.')
  ];
  $form = system_settings_form($form);
  return $form;
}
