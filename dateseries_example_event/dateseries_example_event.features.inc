<?php
/**
 * @file
 * dateseries_example_event.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dateseries_example_event_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function dateseries_example_event_node_info() {
  $items = array(
    'fiw_event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
