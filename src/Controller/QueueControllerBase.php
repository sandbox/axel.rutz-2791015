<?php
/**
 * @file QueueCntroller.php
 */

namespace Drupal\dateseries\Controller;

use Drupal\dateseries\CRUDQueue\Queue;
use Drupal\dateseries\CRUDQueue\QueueControllerInterface;
use Drupal\dateseries\CRUDQueue\QueueItemInterface;
use Drupal\dateseries\Model\Event;
use Drupal\dateseries\Model\Series;
use Drupal\dateseries\Tools\DateseriesLogger;
use Drupal\dateseries\Tools\Logger;

/**
 * Class QueueController
 * @package Drupal\dateseries\Controller
 *
 * The QueueController knows what to do when evetns change and controls the
 * general purpose Queue.
 */
abstract class QueueControllerBase implements QueueControllerInterface {
  /** @var SeriesControllerInterface */
  protected $controller;
  /** @var QueueControllerSettings */
  protected $settings;

  /** @var object $saved_series_entity_raw */
  protected $saved_series_entity_raw = NULL;

  /**
   * QueueController constructor.
   * @param SeriesControllerInterface $controller
   */
  public function __construct(SeriesControllerInterface $controller, QueueControllerSettings $settings) {
    $this->controller = $controller;
    $this->settings = $settings;
    if ($series = $this->controller->getEvent()->getSeries()) {
      $this->saved_series_entity_raw = $series->getEntity()->value();
    }
  }

  public function beforeFlushQueue(Queue $queue) {
    // * - create a series if needed
    $series = $this->controller->getEvent()->getSeries();
    $series_is_needed = count($queue->getEffectiveItems()) > 1;
    $series_shall_be_created = $series_is_needed || $this->settings->isSeriesCreatedForSingleEvent();
    if (!$series && $series_shall_be_created) {
      $series = $this->controller->getEvent()->createSeries();
      DateseriesLogger::logSeries($series, 'Series is autocreated: !series', Logger::INFO);
    }

    // We don't need this if the event is alone and no series was created.
    if ($series && $series->hasFieldDates()) {
      $dates = $this->settings->getUpdater()->getUpdatedSeriesDates($queue);
      if (isset($dates)) {
        $series->setFieldDates($dates);
      }
      $this->saveSeriesIfNeeded($series);
      DateseriesLogger::logSeries($series, 'Series is saved: !series', Logger::INFO);
    }

    // Now that it has an ID, set the series.
    if ($series) {
      $this->controller->getEvent()->setSeries($series);

      /** @var Event $current_event */
      foreach ($queue->getItemsToSave() as $current_event) {
        $current_event->setSeries($series);
      }
    }
  }

  public function whileFlushQueueBeforeDelete(Queue $queue) {
    // Log after create, but before delete.
    if ($queue->hasQueuedItems()) {
      DateseriesLogger::logQueue($queue, 'Created: !create. Updated: !update. Deleted: !delete.', Logger::INFO);

      $args = DateseriesLogger::getQueuePlaceholders($queue);
      drupal_set_message(t('Date series processed. Created !create. Updated !update. Deleted !delete.', $args));
    }

  }

  public function afterFlushQueue(Queue $queue) {
    // * If no events left in series, delete the series
    $series = $this->controller->getEvent()->getSeries();
    $effective_events = $queue->getEffectiveItems();
    $series_shall_be_deleted = !$effective_events ||
      (!$this->settings->isSeriesKeptForSingleEvent() && count($effective_events) <= 1);
    if ($series && $series_shall_be_deleted) {
      DateseriesLogger::logSeries($series, 'The series is deleted: !series', Logger::INFO);

      $series->deleteUnprocessed();
    }
  }

  /**
   * Get stored events, keyed by entity id.
   *
   * @param bool $uncached
   * @return Event[]
   */
  public function getStoredItems($uncached = FALSE) {
    /** @var Event[] $events */
    static $events;
    if (!isset($events) || $uncached) {
      $events = [];
      // No ID? New series, so no results.
      if (
        ($series = $this->controller->getEvent()->getSeries())
        && ($target_id = $series->getId())
      ) {
        $field_name = $this->controller->getEvent()->getEntityController()->getEventFieldSeries();

        $query = new \EntityFieldQuery();
        $query->fieldCondition($field_name, 'target_id', $target_id, '=');
        $result = $query->execute();

        // Process the query result.
        foreach ($result as $entity_type => $entity_stubs) {
          foreach ($entity_stubs as $entity_id => $_) {
            // This assumes that we only have one entity type.
            $events[$entity_id] = new Event(
              $this->controller->getEvent()->getEntityController(),
              entity_metadata_wrapper($entity_type, $entity_id)
            );
          }
        }
      }
    }
    return $events;
  }

  /**
   * Get unique event keys, use object hash for unsaved entities.
   *
   * @param Event $event
   * @return int|string
   */
  public function getItemKey($event) {
    if ($id = $event->getId()) {
      return $id;
    }
    else {
      $object_id = spl_object_hash($event->getEntity()->value());
      return '#' . $object_id;
    }
  }

  /**
   * @param Event $event
   * @return boolean
   */
  public function checkItemType($event) {
    return $event instanceof Event;
  }

  /**
   * @param Event $event
   * @return boolean
   */
  public function isUnsavedItem($event) {
    return !$event->getId();
  }

  /**
   * @param Event $event
   */
  public function saveItem($event) {
    $event->saveUnprocessed();
  }

  /**
   * @param Event $event
   */
  public function deleteItem($event) {
    $event->deleteUnprocessed();
  }

  /**
   * @param Series $series
   */
  protected function saveSeriesIfNeeded(Series $series) {
    // * - save the series if needed
    $series_has_changed = $series->getEntity()
        ->value() !== $this->saved_series_entity_raw;
    if ($series_has_changed) {
      $series->saveUnprocessed();

      DateseriesLogger::logSeries($series, 'Series is saved: !series', Logger::INFO);
    }
  }
}