<?php
/**
 * @file SeriesCreator.php
 */

namespace Drupal\dateseries\Controller;


use Drupal\dateseries\Model\Event;
use Drupal\dateseries\Model\Series;
use Drupal\dateseries\Tools\EntityTools;

class EntityControllerDefault implements EntityControllerInterface {
  /** @var string */
  protected $series_entity_type;
  /** @var string */
  protected $series_entity_bundle;
  /** @var string|null */
  protected $series_field_dates;

  /** @var string|null */
  protected $event_entity_type;
  /** @var string|null */
  protected $event_entity_bundle;
  /** @var string */
  protected $event_field_series;
  /** @var string */
  protected $event_field_date;
  /** @var string|null */
  protected $event_field_dates;
  /** @var string|null */
  protected $event_field_add;

  /** @var QueueControllerSettings */
  protected $queue_controller_settings;

  /**
   * EntityControllerDefault constructor.
   *
   * @param string $series_entity_type
   * @param string $series_entity_bundle
   * @param string|null $series_field_dates
   * @param string|null $event_entity_type
   * @param string|null $event_entity_bundle
   * @param string $event_field_series
   * @param string $event_field_date
   * @param string|null $event_field_dates
   * @param string|null $event_field_add
   * @param QueueControllerSettings|null $queue_controller_settings
   */
  public function __construct(
    $series_entity_type,
    $series_entity_bundle,
    $series_field_dates,
    $event_entity_type,
    $event_entity_bundle,
    $event_field_series,
    $event_field_date,
    $event_field_dates,
    $event_field_add,
    $queue_controller_settings = NULL
  ) {
    $this->series_entity_type = $series_entity_type;
    $this->series_entity_bundle = $series_entity_bundle;
    $this->series_field_dates = $series_field_dates;
    $this->event_entity_type = $event_entity_type;
    $this->event_entity_bundle = $event_entity_bundle;
    $this->event_field_series = $event_field_series;
    $this->event_field_date = $event_field_date;
    $this->event_field_dates = $event_field_dates;
    $this->event_field_add = $event_field_add;
    if ($queue_controller_settings) {
      $this->queue_controller_settings = $queue_controller_settings;
    }
    else {
      $this->queue_controller_settings = new QueueControllerSettings();
    }
  }

  /**
   * Wrap an event entity.
   *
   * @param \EntityDrupalWrapper $entity
   * @return Event|null
   */
  public function wrapEvent(\EntityDrupalWrapper $entity) {
    if ($this->isEventEntity($entity)) {
      return new Event($this, $entity);
    }
    else {
      return NULL;
    }
  }

  /**
   * Wrap a series entity.
   *
   * @param \EntityDrupalWrapper $entity
   * @return Series|null
   */
  public function wrapSeries(\EntityDrupalWrapper $entity) {
    if ($this->isSeriesEntity($entity)) {
      return new Series($this, $entity);
    }
    else {
      return NULL;
    }
  }

  /**
   * Check if entity is series.
   *
   * @param \EntityDrupalWrapper $entity
   * @return bool
   */
  public function isSeriesEntity(\EntityDrupalWrapper $entity) {
    return $entity->type === $this->series_entity_type
      && $entity->getBundle() === $this->series_entity_bundle;
  }

  /**
   * Check if entity is event.
   *
   * It must have series and date field. Other fields are optional.
   * Bundle is checked only when given.
   *
   * @param \EntityDrupalWrapper $entity
   * @return bool
   */
  public function isEventEntity(\EntityDrupalWrapper $entity) {
    $check_entity_type = $this->event_entity_type ? ($entity->type === $this->event_entity_type) : TRUE;
    $check_entity_bundle = $this->event_entity_bundle ? ($entity->getBundle() === $this->event_entity_bundle) : TRUE;
    $check_field_series = EntityTools::entityHasField($entity, $this->event_field_series);
    $check_field_date = EntityTools::entityHasField($entity, $this->event_field_date);
    return $check_entity_type && $check_entity_bundle && $check_field_series && $check_field_date;
  }

  /**
   * @return string
   */
  public function getSeriesEntityType() {
    return $this->series_entity_type;
  }

  /**
   * @return string
   */
  public function getSeriesEntityBundle() {
    return $this->series_entity_bundle;
  }

  /**
   * @return null|string
   */
  public function getSeriesFieldDates() {
    return $this->series_field_dates;
  }

  /**
   * @return null|string
   */
  public function getEventEntityType() {
    return $this->event_entity_type;
  }

  /**
   * @return null|string
   */
  public function getEventEntityBundle() {
    return $this->event_entity_bundle;
  }

  /**
   * @return string
   */
  public function getEventFieldSeries() {
    return $this->event_field_series;
  }

  /**
   * @return string
   */
  public function getEventFieldDate() {
    return $this->event_field_date;
  }

  /**
   * @return null|string
   */
  public function getEventFieldDates() {
    return $this->event_field_dates;
  }

  /**
   * @return null|string
   */
  public function getEventFieldAdd() {
    return $this->event_field_add;
  }

  /**
   * @return \Drupal\dateseries\Controller\QueueControllerSettings
   */
  public function getQueueControllerSettings() {
    return $this->queue_controller_settings;
  }

}
