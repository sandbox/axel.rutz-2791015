<?php
/**
 * @file ControllerBase.php
 */

namespace Drupal\dateseries\Controller;

use Drupal\dateseries\Factory;
use Drupal\dateseries\Model\Event;
use Drupal\dateseries\CRUDQueue\Queue;
use Drupal\dateseries\Tools\DateseriesLogger;
use Drupal\dateseries\Tools\FixListWrappers;
use Drupal\dateseries\Tools\ListTools;

/**
 * Class SeriesControllerBase
 * @package Drupal\dateseries\Controller
 *
 * Series base controller. The import controller subclass deletes the whole
 * series on event delete, while the edit controller does not.
 */
class SeriesControllerBase implements SeriesControllerInterface {

  /** @var Event */
  protected $event;

  public function __construct(Event $event) {
    $this->event = $event;
  }

  /**
   * @return Event
   */
  public function getEvent() {
    return $this->event;
  }

  public function hookPreSave() {
    $qc_settings = $this->event->getEntityController()->getQueueControllerSettings();
    $queue = new Queue(new QueueControllerForPreSave($this, $qc_settings));
    // Set dates
    if (($dates_to_set_field = $this->event->getFieldDates()) && $dates_to_set_field->value()) {
      $dates_to_set = ListTools::cloneListItems($dates_to_set_field);
      FixListWrappers::setList($dates_to_set_field, []);
      DateseriesLogger::logDates($dates_to_set, 'Found dates to set: %dates');
      $this->setDates($queue, $dates_to_set);
    }

    // Add dates
    if (($dates_to_add_field = $this->event->getFieldAdd()) && $dates_to_add_field->value()) {
      $dates_to_add = ListTools::cloneListItems($dates_to_add_field);
      FixListWrappers::setList($dates_to_add_field, []);
      DateseriesLogger::logDates($dates_to_add, 'Found dates to add: %dates');
      $queue->addItemsToSave($this->event->replicateForDates($dates_to_add));

    }
    // The Queue will flush on destruction and do everything.
  }

  public function hookPreDelete() {
    $qc_settings = $this->event->getEntityController()->getQueueControllerSettings();
    $queue = new Queue(new QueueControllerForPreDelete($this, $qc_settings));
    // The Queue will flush on destruction and do everything.
  }


  /**
   * Set dates.
   *
   * We assume here that dates contains at lease one event.
   * To set-no-dates consistently would require additional logic to delete
   * the event it self that we are processing presave for.
   * We don't need this anyway as correctly configured feeds imports
   * won't set empty dates field, but instead delete the event.
   *
   * @param Queue $queue
   * @param \EntityMetadataWrapper[] $dates
   */
  protected function setDates(Queue $queue, array $dates) {
    if (!$dates) {
      return;
    }
    $current_events = $queue->getEffectiveItems();

    // Events that need not be updated are sorted out.
    $current_dates = array_map(function (Event $event) {
      return $event->getFieldDate();
    }, $current_events);
    $dates_todo = ListTools::diffWrapperArrays($dates, $current_dates, ['value', 'value2']);
    $current_dates_todo = ListTools::diffWrapperArrays($current_dates, $dates, ['value', 'value2']);
    $events_todo = array_intersect_key($current_events, $current_dates_todo);

    $this->setCurrentEventDate($queue, $events_todo, $dates_todo);
    $this->setEventDates($queue, $events_todo, $dates_todo);
    $queue->addItemsToSave($this->event->replicateForDates($dates_todo));
    $queue->addItemsToDelete($events_todo);
  }

  /**
   * Set current event date if itÄs in the dates to set.
   *
   * @param Queue $queue
   * @param $events_to_set
   * @param $dates_to_set
   */
  protected function setCurrentEventDate(Queue $queue, &$events_to_set, &$dates_to_set) {
    // If the current event has not been sorted out,
    // the first date is assigned to the current event
    $active_event_key = $queue->getController()->getItemKey($this->event);
    if (isset($events_to_set[$active_event_key])) {
      $event_date = array_shift($dates_to_set);
      $this->event->setFieldDate($event_date);
      unset($events_to_set[$active_event_key]);
    }
  }

  /**
   * Set dates on events, remove from arrays and add to queue.
   *
   * @param Queue $queue
   * @param $events
   * @param $dates
   */
  protected function setEventDates(Queue $queue, &$events, &$dates) {
    while ($events && $dates) {
      $current_event = array_shift($events);
      $current_date = array_shift($dates);
      $current_event->setFieldDate($current_date);
      $queue->addItemsToSave([$current_event]);
    }
  }

}
