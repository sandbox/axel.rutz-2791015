<?php
/**
 * @file QueueControllerForPreDelete.php
 */

namespace Drupal\dateseries\Controller;


use Drupal\dateseries\CRUDQueue\QueueControllerInterface;
use Drupal\dateseries\Model\Event;

class QueueControllerForPreDelete extends QueueControllerBase implements QueueControllerInterface {
  /**
   * @param Event[] $events
   */
  public function adjustStoredItems(array &$events) {
    $event = $this->controller->getEvent();
    unset($events[$this->getItemKey($event)]);
  }
}
