<?php
/**
 * @file SeriesControllerImport.php
 */

namespace Drupal\dateseries\Controller;


use Drupal\dateseries\CRUDQueue\Queue;
use Drupal\dateseries\Model\Event;

class SeriesControllerForImport extends SeriesControllerBase {
  public function hookPreDelete() {
    // In import mode, we delete the all series events on event delete.
    // The queue will then also delete the series.
    $qc_settings = $this->event->getEntityController()->getQueueControllerSettings();
    $queue = new Queue(new QueueControllerForPreDelete($this, $qc_settings));
    $events = $queue->getEffectiveItems();
    $queue->addItemsToDelete($events);
  }
}
