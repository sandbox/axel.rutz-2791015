<?php
/**
 * @file QueueControllerSettings.php
 */

namespace Drupal\dateseries\Controller;


use Drupal\dateseries\DatesUpdater\DatesUpdaterInterface;
use Drupal\dateseries\DatesUpdater\DatesUpdaterSimple;

class QueueControllerSettings {
  /** @var  boolean */
  protected $series_created_for_single_event = FALSE;
  /** @var  boolean */
  protected $series_kept_for_single_event = FALSE;
  /** @var DatesUpdaterInterface */
  protected $updater;

  /**
   * @return boolean
   */
  public function isSeriesCreatedForSingleEvent() {
    return $this->series_created_for_single_event;
  }

  /**
   * @return boolean
   */
  public function isSeriesKeptForSingleEvent() {
    return $this->series_kept_for_single_event;
  }

  /**
   * @return DatesUpdaterInterface
   */
  public function getUpdater() {
    if (!$this->updater) {
      $this->updater = new DatesUpdaterSimple();
    }
    return $this->updater;
  }

  /**
   * @param boolean $series_created_for_single_event
   */
  public function setSeriesCreatedForSingleEvent($series_created_for_single_event) {
    $this->series_created_for_single_event = $series_created_for_single_event;
  }

  /**
   * @param boolean $series_kept_for_single_event
   */
  public function setSeriesKeptForSingleEvent($series_kept_for_single_event) {
    $this->series_kept_for_single_event = $series_kept_for_single_event;
  }

  /**
   * @param DatesUpdaterInterface $updater
   */
  public function setUpdater($updater) {
    $this->updater = $updater;
  }

}
