<?php
/**
 * @file QueueControllerForChange.php
 */

namespace Drupal\dateseries\Controller;


use Drupal\dateseries\CRUDQueue\QueueControllerInterface;
use Drupal\dateseries\Model\Event;

class QueueControllerForPreSave extends QueueControllerBase implements QueueControllerInterface {
  /**
   * @param Event[] $events
   */
  public function adjustStoredItems(array &$events) {
    $event = $this->controller->getEvent();
    $events[$this->getItemKey($event)] = $event;
  }
}
