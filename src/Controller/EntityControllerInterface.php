<?php
/**
 * @file EntityControllerInterface.php
 */
namespace Drupal\dateseries\Controller;

use Drupal\dateseries\Model\Event;
use Drupal\dateseries\Model\Series;

interface EntityControllerInterface {
  /**
   * Wrap an event entity.
   *
   * @param \EntityDrupalWrapper $entity
   * @return Event|null
   */
  public function wrapEvent(\EntityDrupalWrapper $entity);

  /**
   * Wrap a series entity.
   *
   * @param \EntityDrupalWrapper $entity
   * @return Series|null
   */
  public function wrapSeries(\EntityDrupalWrapper $entity);

  /**
   * Check if entity is series.
   *
   * @param \EntityDrupalWrapper $entity
   * @return bool
   */
  public function isSeriesEntity(\EntityDrupalWrapper $entity);

  /**
   * Check if entity is event.
   *
   * It must have series and date field. Other fields are optional.
   * Bundle is checked only when given.
   *
   * @param \EntityDrupalWrapper $entity
   * @return bool
   */
  public function isEventEntity(\EntityDrupalWrapper $entity);

  /**
   * @return string
   */
  public function getSeriesEntityType();

  /**
   * @return string
   */
  public function getSeriesEntityBundle();

  /**
   * @return null|string
   */
  public function getSeriesFieldDates();

  /**
   * @return null|string
   */
  public function getEventEntityType();

  /**
   * @return null|string
   */
  public function getEventEntityBundle();

  /**
   * @return string
   */
  public function getEventFieldSeries();

  /**
   * @return string
   */
  public function getEventFieldDate();

  /**
   * @return null|string
   */
  public function getEventFieldDates();

  /**
   * @return null|string
   */
  public function getEventFieldAdd();

  public function getQueueControllerSettings();

}