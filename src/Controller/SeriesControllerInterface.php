<?php
/**
 * @file ControllerInterface.php
 */
namespace Drupal\dateseries\Controller;

use Drupal\dateseries\Model\Event;
use Drupal\dateseries\Model\Series;
use Drupal\dateseries\CRUDQueue\Queue;


/**
 * Interface SeriesControllerInterface
 * @package Drupal\dateseries\Controller
 */
interface SeriesControllerInterface {
  public function __construct(Event $event);

  /**
   * @return Event
   */
  public function getEvent();

  public function hookPreSave();

  public function hookPreDelete();
}
