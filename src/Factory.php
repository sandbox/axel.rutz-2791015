<?php
namespace Drupal\dateseries;

use Drupal\dateseries\Controller\SeriesControllerForEdit;
use Drupal\dateseries\Controller\SeriesControllerForImport;
use Drupal\dateseries\Controller\SeriesControllerInterface;
use Drupal\dateseries\Controller\EntityControllerDefault;
use Drupal\dateseries\Controller\EntityControllerInterface;
use Drupal\dateseries\DatesUpdater\DatesUpdaterInterface;
use Drupal\dateseries\DatesUpdater\DatesUpdaterSimple;
use Drupal\dateseries\Model\Event;
use Drupal\dateseries\Tools\ImportMode;

/**
 * @file Factory.php
 */
class Factory {
  /**
   * @return EntityControllerInterface[]
   */
  public static function getEntityControllers() {
    $default_controller = new EntityControllerDefault(
      'dateseries', // Series entity type
      'dateseries', // Series entity bundle
      'dateseries_dates', // Series field dates
      NULL, // Event entity type (null = any)
      NULL, // Event entity bundle (null = any)
      'dateseries_series', // Event field series
      'dateseries_date', // Event field date
      'dateseries_dates', // Event field dates
      'dateseries_add' // Event field add
    );
    $controllers = [$default_controller];
    drupal_alter('dateseries_entity_controllers', $controllers);
    return $controllers;
  }

  /**
   * @param Event $event
   * @return SeriesControllerInterface
   */
  public static function getController(Event $event) {
    return ImportMode::isActive() ? new SeriesControllerForImport($event) : new SeriesControllerForEdit($event);
  }

}
