<?php
/**
 * @file ImportMode.php
 */

namespace Drupal\dateseries\Tools;


class ImportMode {
  /** @var bool */
  protected static $active;

  /**
   * @return boolean
   */
  public static function isActive() {
    return self::$active;
  }

  /**
   * @param boolean $active
   */
  public static function setActive($active) {
    self::$active = $active;
  }

}