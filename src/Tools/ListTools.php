<?php
/**
 * @file Tools.php
 */

namespace Drupal\dateseries\Tools;




class ListTools {

  /**
   * Convert a traversable (EntityListWrapper) to an array of its item wrappers.
   *
   * @param \Traversable $list
   * @return \EntityMetadataWrapper[]
   */
  public static function getFieldItemWrappers(\Traversable $list) {
    $array = [];
    foreach ($list as $item) {
      $array[] = $item;
    }
    return $array;
  }

  /**
   * Clone a list wrapper's items.
   *
   * @param \EntityListWrapper $list
   * @return \EntityMetadataWrapper[]
   */
  public static function cloneListItems(\EntityListWrapper $list) {
    $values = [];
    /** @var \EntityStructureWrapper $item */
    foreach ($list as $key => $item) {
      //$values[$key] = clone $item;
      // New wrapper with a clone of its data.
      $data = $item->value();
      $info = $item->getPropertyInfo();
      $values[$key] = entity_metadata_wrapper(NULL, $data, ['property info' => $info]);
    }
    return $values;
  }


  /**
   * Intersect arrays of wrapped field content.
   *
   * Outputs elements of array1 that are also in array2.
   * Normalizes the values before comparing.
   *
   * @param \EntityStructureWrapper[] $array1
   * @param \EntityStructureWrapper[] $array2
   * @param array|null $keys
   * @return \EntityStructureWrapper[]
   */
  public static function intersectWrapperArrays(array $array1, array $array2, array $keys = NULL) {
    $diff = array_uintersect($array1, $array2, function ($v1, $v2) use ($keys) {
      return \Drupal\dateseries\Tools\CompareFieldWrappers::compareNormalized($v1, $v2, $keys);
    });
    return $diff;
  }

  /**
   * Diff arrays of wrapped field content.
   *
   * Outputs elements of array1 that are not in array2.
   * Normalizes the values before comparing.
   *
   * @param \EntityStructureWrapper[] $array1
   * @param \EntityStructureWrapper[] $array2
   * @param array|null $keys
   * @return \EntityStructureWrapper[]
   */
  public static function diffWrapperArrays(array $array1, array $array2, array $keys = NULL) {
    $diff = array_udiff($array1, $array2, function ($v1, $v2) use ($keys) {
      return \Drupal\dateseries\Tools\CompareFieldWrappers::compareNormalized($v1, $v2, $keys);
    });
    return $diff;
  }

}
