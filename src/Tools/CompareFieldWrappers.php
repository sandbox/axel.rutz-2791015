<?php
/**
 * @file CompareFieldWrappers.php
 */

namespace Drupal\dateseries\Tools;


class CompareFieldWrappers {

  /**
   * Compare field wrappers.
   *
   * THis must also work when a field wrapper is NULL.
   *
   * @param \EntityMetadataWrapper|null $wrapper1
   * @param \EntityMetadataWrapper|null $wrapper2
   * @param array|null $keys
   * @return bool
   */
  public static function compareNormalized(\EntityMetadataWrapper $wrapper1 = NULL, \EntityMetadataWrapper $wrapper2 = NULL, array $keys = NULL) {
    $is_list1 = $wrapper1 instanceof \EntityListWrapper;
    $is_list2 = $wrapper2 instanceof \EntityListWrapper;

    if ($is_list1 !== $is_list2) {
      throw new \UnexpectedValueException('Can not compare list and non-list.');
    }

    // Get the wrapper's value if it is not null.
    $value1 = $wrapper1 ? $wrapper1->value() : $wrapper1;
    $value2 = $wrapper2 ? $wrapper2->value() : $wrapper2;

    $additional_levels = $is_list1 ? 1 : 0;
    if ($keys) {
      $keys = array_fill_keys($keys, TRUE);
    }
    else {
      $keys = self::getCommonArrayKeys($value1, $value2, $additional_levels);
    }
    $filtered1 = self::filterArrayKeys($value1, $keys, $additional_levels);
    $filtered2 = self::filterArrayKeys($value2, $keys, $additional_levels);
    $difference = Compare::compareAnyType($filtered1, $filtered2);
    return $difference;
  }

  /**
   * Filter field array keys.
   *
   * Make keys canonical, but only if not NULL or empty.
   *
   * @param array|null $array
   * @param array|null $keys
   * @param int $additional_levels
   * @return array|null
   */
  public static function filterArrayKeys(array $array = NULL, array $keys = NULL, $additional_levels = 0) {
    if ($array && $keys) {
      if ($additional_levels > 0) {
        $return = array_map(function ($array) use ($keys) {
          return self::filterArrayKeys($array, $keys);
        }, $array);
        $return = array_values($return);
      }
      else {
        $return = array_intersect_key($array, $keys);
      }
      ksort($return);
      return $return;
    }
    else {
      return $array;
    }
  }

  /**
   * Guess canonical common field array keys.
   *
   * Fields sometimes add additional values which mess up our comparison.
   *
   * @param array|null $a1
   * @param array|null $a2
   * @param int $additional_levels
   * @return array|null
   */
  public static function getCommonArrayKeys(array $a1 = NULL, array $a2 = NULL, $additional_levels = 0) {
    if ($a1 && $a2) {
      if ($additional_levels > 0) {
        // Use first element of each.
        $return = self::getCommonArrayKeys(reset($a1), reset($a2), $additional_levels - 1);
      }
      else {
        $return = array_intersect_key($a1, $a2);
      }
      return $return;
    }
    else {
      return NULL;
    }
  }
}