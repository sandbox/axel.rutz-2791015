<?php
/**
 * @file Logger.php
 */

namespace Drupal\dateseries\Tools;

/**
 * Class Logger
 * @package Drupal\dateseries\Tools
 */
class Logger {
  // Types
  const DEBUG = 'debug';
  const INFO = 'info';
  const WARNING = 'warning';
  const ERROR = 'error';

  const LOG_ERRORS = 0;
  const LOG_STATUS = 1;
  const LOG_DEBUG = 2;

  public static function logLevelOptions() {
    return [
      self::LOG_ERRORS => t('Log only errors to watchdog.'),
      self::LOG_STATUS => t('Log errors and status messages to watchdog.'),
      self::LOG_DEBUG => t('Log errors, status messages and debug messages to watchdog.'),
    ];
  }

  public static function log($message, $vars = [], $type = self::INFO) {
    $log_level = variable_get('dateseries_log_level');
    $show_in_ui = user_access('dateseries:show debug messages');
    if ($type === self::DEBUG) {
      $ui_type = 'status';
      $log_to_watchdog = $log_level >= self::LOG_DEBUG;
      $wd_severity = WATCHDOG_DEBUG;
    }
    elseif ($type === self::INFO) {
      $ui_type = 'status';
      $log_to_watchdog = $log_level >= self::LOG_STATUS;
      $wd_severity = WATCHDOG_INFO;
    }
    elseif ($type === self::WARNING) {
      $ui_type = 'warning';
      $log_to_watchdog = $log_level >= self::LOG_STATUS;
      $wd_severity = WATCHDOG_WARNING;
    }
    elseif ($type === self::ERROR) {
      $ui_type = 'error';
      $log_to_watchdog = TRUE; // Always log error messages.
      $wd_severity = WATCHDOG_ERROR;
    }
    else {
      return FALSE;
    }

    if ($show_in_ui) {
      // No translation for debug messages intended.
      drupal_set_message('DEBUG Date Series: ' . format_string($message, $vars), $ui_type);
    }
    if ($log_to_watchdog) {
      watchdog('dateseries', $message, $vars, $wd_severity);
    }
    return TRUE;
  }

}
