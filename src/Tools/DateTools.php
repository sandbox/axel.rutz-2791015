<?php
/**
 * @file DateTools.php
 */

namespace Drupal\dateseries\Tools;


class DateTools {

  /**
   * Format a list of wrapped dates.
   *
   * Dates may be null.
   *
   * @param  \EntityListWrapper|\EntityStructureWrapper[] $dates
   * @return string[]
   */
  public static function formatDateWrappers($dates) {
    $return = [];
    foreach ($dates as $key => $date) {
      $return[$key] = self::formatDateWrapper($date);
    }
    return $return;
  }

  /**
   * @param \EntityStructureWrapper $date
   * @return string
   */
  public static function formatDateWrapper(\EntityStructureWrapper $date) {
    if ($date) {
      try {
        /** @var \EntityMetadataWrapper $wrapper1 */
        $wrapper1 = $date->get('value');
        $timestamp1 = $wrapper1->value();
        $formatted = format_date($timestamp1, 'short');
      } catch (\EntityMetadataWrapperException $e) {
        $formatted = t('(No date)');
      }
      try {
        /** @var \EntityMetadataWrapper $wrapper2 */
        $wrapper2 = $date->get('value2');
        $timestamp2 = $wrapper2->value();
        $formatted .= '...' . format_date($timestamp2, 'short');
      } catch (\EntityMetadataWrapperException $e) {
      }
      return $formatted;
    }
    else {
      $formatted = t('(No date)');
      return $formatted;
    }
  }

  /**
   * Filter out empty dates.
   *
   * @param \EntityListWrapper|\EntityStructureWrapper[] $field
   * @return \EntityListWrapper
   */
  public static function filterOutEmptyDates($field) {
    /** @var \EntityMetadataWrapper $item */
    foreach ($field as $key => $item) {
      if (empty($item->value()) || empty($item->value()['value'])) {
        $field->offsetUnset($key);
      }
    }
    return $field;
  }

}