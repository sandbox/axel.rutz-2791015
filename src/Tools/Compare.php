<?php
/**
 * @file Compare.php
 */

namespace Drupal\dateseries\Tools;


class Compare {

  /**
   * @param $value1
   * @param $value2
   * @return int
   */
  public static function compareAnyType($value1, $value2) {
    $serialized1 = serialize($value1);
    $serialized2 = serialize($value2);
    $difference = self::compare($serialized1, $serialized2);
    return $difference;
  }

  /**
   * Emulate PHP7's spaceship.
   *
   * @param $serialized1
   * @param $serialized2
   * @return int
   */
  public static function compare($serialized1, $serialized2) {
    $difference = ($serialized1 === $serialized2) ? 0 :
      (($serialized1 > $serialized2) ? 1 : -1);
    return $difference;
  }
}