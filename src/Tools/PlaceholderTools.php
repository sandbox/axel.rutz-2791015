<?php
/**
 * @file EntityTools.php
 */

namespace Drupal\dateseries\Tools;


class PlaceholderTools {

  /**
   * Collect useful replacements for entities.
   *
   * Note that wrappers may contain NULL.
   *
   * @param \EntityDrupalWrapper[] $entities
   * @return array
   */
  public static function entityPlaceholders(array $entities) {
    $args = [];
    foreach ($entities as $key => $entity) {
      $label = $entity->value() ? $entity->label() : t('(None)');
      $args["@{$key}_label"] = $label;

      $id = $entity->getIdentifier();
      $args["@{$key}_id"] = $id ? $id : t('(None)');

      if ($entity->value()) {
        $url_options = entity_uri($entity->type(), $entity->value());
        $url = url($url_options['path'], $url_options);
        $args["@{$key}_url"] = $url;
        $args["!{$key}"] = l($label, $url_options['path'], $url_options);
      }
      else {
        $args["@{$key}_url"] =  t('(None)');
        $args["!{$key}"] =  t('(None)');
      }

    }
    return $args;
  }

  /**
   * Provide links for entities, concatenated.
   *
   * @param \EntityDrupalWrapper[] $entities
   * @param string[] $labels
   * @param string $separator
   * @return string
   */
  public static function entityLinks(array $entities, array $labels = NULL, $separator = ', ') {
    $links = self::entityLinkArray($entities, $labels);
    $return = $links ? implode($separator, $links) : t('None');
    return $return;
  }

  /**
   * Provide links for entities.
   *
   * @param \EntityDrupalWrapper[] $entities
   * @param string[] $labels
   * @return array
   */
  public static function entityLinkArray(array $entities, array $labels = NULL) {
    $links = [];
    foreach ($entities as $key => $entity) {
      $label = !empty($labels[$key]) ? $labels[$key] : $entity->label();
      if ($entity->getIdentifier()) {
        $url_options = entity_uri($entity->type(), $entity->value());
        $links[$key] = l($label, $url_options['path'], $url_options);
      }
      else {
        $links[$key] = $label;
      }
    }
    return $links;
  }

  /**
   * Collect useful replacements for entities.
   *
   * @param \EntityListWrapper|\EntityStructureWrapper[] $dates
   * @param string $separator
   * @return string
   */
  public static function dates($dates, $separator = ', ') {
    $strings = DateTools::formatDateWrappers($dates);
    return implode($separator, $strings);
  }

}
