<?php
/**
 * @file FixListWrappers.php
 */

namespace Drupal\dateseries\Tools;


class FixListWrappers {

  /**
   * Empty a list, workaround a bug in EntityListWrapper.
   *
   * @param \EntityListWrapper $field
   */
  public static function clearList(\EntityListWrapper $field) {
    foreach ($field as $key => $_) {
      $field->offsetUnset($key);
    }
  }

  /**
   * Copy a list, workaround a bug in EntityListWrapper.
   *
   * @param \EntityListWrapper $target
   * @param \EntityListWrapper|\EntityStructureWrapper[] $value
   */
  public static function setList(\EntityListWrapper $target, $value) {
    FixListWrappers::clearList($target);
    self::appendListToList($target, $value);

  }

  /**
   * @param \EntityListWrapper $list
   * @param \EntityListWrapper|\EntityStructureWrapper[] $append
   */
  public static function appendListToList(\EntityListWrapper $list, $append) {
    /** @var \EntityMetadataWrapper $item */
    foreach ($append as $key => $item) {
      self::appendItemToList($list, $item);
    }
  }

  /**
   * @param \EntityListWrapper $list
   * @param \EntityMetadataWrapper $item
   */
  public static function appendItemToList(\EntityListWrapper $list, \EntityMetadataWrapper $item) {
    $list[] = $item->value();
  }
}