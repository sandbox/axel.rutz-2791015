<?php
/**
 * @file EntityTools.php
 */

namespace Drupal\dateseries\Tools;


class EntityTools {

  /**
   * Checks that the entity wrapper has a property.
   *
   * @param \EntityDrupalWrapper $entity
   * @param string $field_name
   * @return bool
   */
  public static function entityHasField(\EntityDrupalWrapper $entity, $field_name) {
    return array_key_exists($field_name, $entity->getPropertyInfo());
  }

  /**
   * Create an empty entity of some bundle.
   *
   * @param string $entity_type
   * @param string|null $bundle
   * @return object
   */
  public static function createEntityRaw($entity_type, $bundle = NULL) {
    $values = [];
    if (
      $bundle
      && ($info = entity_get_info($entity_type))
      && !empty($info['entity keys']['bundle'])
    ) {
      $bundle_key = $info['entity keys']['bundle'];
      $values[$bundle_key] = $bundle;
    }
    $entity_raw = entity_create($entity_type, $values);
    return $entity_raw;
  }

  /**
   * Create an empty entity of some bundle and return the wrapper.
   *
   * @param string $entity_type
   * @param string|null $bundle
   * @return \EntityDrupalWrapper
   */
  public static function createEntityWrapped($entity_type, $bundle = NULL) {
    $entity_raw = self::createEntityRaw($entity_type, $bundle);
    $entity_wrapped = entity_metadata_wrapper($entity_type, $entity_raw);
    return $entity_wrapped;
  }

}