<?php
/**
 * @file EventTools.php
 */

namespace Drupal\dateseries\Tools;


use Drupal\dateseries\CRUDQueue\Queue;
use Drupal\dateseries\Model\Event;
use Drupal\dateseries\Model\Series;

class DateseriesLogger {

  /**
   * Log dates with placeholder &dates.
   *
   * @param \EntityStructureWrapper[] $dates
   * @param string $message
   * @param $loglevel
   */
  public static function logDates($dates, $message, $loglevel = Logger::DEBUG) {
    $args = ['%dates' => PlaceholderTools::dates($dates)];
    Logger::log($message, $args, $loglevel);
  }

  /**
   * Log event with placeholder !event.
   *
   * @param $event
   * @param $message
   */
  public static function logEvent($event, $message, $loglevel = Logger::DEBUG) {
    $args = DateseriesLogger::getEventPlaceholder($event);
    Logger::log($message, $args, $loglevel);
  }

  /**
   * Log series with placeholder !series.
   *
   * @param $series
   * @param $message
   */
  public static function logSeries($series, $message, $loglevel = Logger::DEBUG) {
    $args = DateseriesLogger::getSeriesPlaceholder($series);
    Logger::log($message, $args, $loglevel);
  }

  /**
   * Log queue content.
   *
   * @param Queue $queue
   * @param string $message
   * @return \string[]
   */
  public static function logQueue(Queue $queue, $message, $loglevel = Logger::DEBUG) {
    $args = self::getQueuePlaceholders($queue);
    Logger::log($message, $args, $loglevel);
    return $args;
  }

  public static function logPeakMemoryUsage() {
    Logger::log('Peak memory usage: %memory M', ['%memory' => memory_get_peak_usage(TRUE) / 1024 / 1024]);
  }

  /**
   * Get queue placeholders.
   *
   * @param Queue $queue
   * @return string[]
   */
  public static function getQueuePlaceholders(Queue $queue) {
    $args = [
      '!create' => DateseriesLogger::getEventDateLinks($queue->getItemsToCreate()),
      '!update' => DateseriesLogger::getEventDateLinks($queue->getItemsToUpdate()),
      '!delete' => DateseriesLogger::getEventDateLinks($queue->getItemsToDelete()),
    ];
    return $args;
  }

  /**
   * @param Event $event
   * @return array
   */
  public static function getEventPlaceholder(Event $event) {
    return self::getEventDateLinkArray(['!event' => $event]);
  }

  /**
   * @param Series $series
   * @return array
   */
  public static function getSeriesPlaceholder(Series $series) {
    return PlaceholderTools::entityLinkArray(['!series' => $series->getEntity()]);
  }

  /**
   * Provide links for events with date label, concatenated.
   *
   * @param Event[] $events
   * @param string $separator
   * @return string
   */
  public static function getEventDateLinks(array $events, $separator = ', ') {
    $links = self::getEventDateLinkArray($events);
    $return = $links ? implode($separator, $links) : t('(None)');
    return $return;
  }


  /**
   * Provides links for events with date label.
   *
   * @param Event[] $events
   * @return array
   */
  public static function getEventDateLinkArray(array $events) {
    $entities = array_map(function (Event $event) {
      return $event->getEntity();
    }, $events);
    $labels = array_map(function (Event $event) {
      return format_string('!date: !label', [
        '!date' => DateTools::formatDateWrapper($event->getFieldDate()),
        '!label' => $event->getEntity()->label(),
      ]);
    }, $events);
    return PlaceholderTools::entityLinkArray($entities, $labels);
  }
}
