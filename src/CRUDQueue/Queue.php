<?php
/**
 * @file Queue.php
 */

namespace Drupal\dateseries\CRUDQueue;

use Drupal\dateseries\Controller\SeriesControllerInterface;

/**
 * Class Queue
 * @package Drupal\dateseries
 *
 * This Queue class encapsulates entites that will be saved or deleted,
 * and the finally existing items can be queried.
 * All queued operations are performed on instance destruction.
 * It eats items of class QueueItemInterface and is instantiated with a class
 * implementing QueueControlerIntarface that gives the items meaning.
 */
class Queue {
  /** @var QueueControllerInterface */
  protected $controller;

  /** @var QueueItemInterface[] */
  protected $items_to_create = [];
  /** @var QueueItemInterface[] */
  protected $items_to_update = [];
  /** @var QueueItemInterface[] */
  protected $items_to_delete = [];

  /**
   * SeriesController constructor.
   *
   * @param SeriesControllerInterface $controller
   */
  public function __construct(QueueControllerInterface $queue_controller) {
    $this->controller = $queue_controller;
  }

  function __destruct() {
    $this->flushQueue();
  }

  /**
   * Queue items to save.
   *
   * @param QueueItemInterface[] $items
   */
  public function addItemsToSave(array $items) {
    foreach ($items as $item) {
      if (!$this->controller->checkItemType($item)) {
        throw new \UnexpectedValueException('Invalid Queue item.');
      }
      $key = $this->controller->getItemKey($item);
      if ($this->controller->isUnsavedItem($item)) {
        $this->items_to_create[$key] = $item;
      }
      else {
        $this->items_to_update[$key] = $item;
      }
    }
  }

  /**
   * Queue items to delete.
   *
   * @param QueueItemInterface[] $items
   */
  public function addItemsToDelete(array $items) {
    foreach ($items as $item) {
      if (!$this->controller->checkItemType($item)) {
        throw new \UnexpectedValueException('Invalid Queue item.');
      }
      $key = $this->controller->getItemKey($item);
      $this->items_to_delete[$key] = $item;
    }
  }

  /**
   * Get items queued for update.
   *
   * @return QueueItemInterface[]
   */
  public function getItemsToCreate() {
    return $this->items_to_create;
  }

  /**
   * @return QueueItemInterface[]
   */
  public function getItemsToUpdate() {
    return $this->items_to_update;
  }

  /**
   * @return QueueItemInterface[]
   */
  public function getItemsToSave() {
    return $this->items_to_create + $this->items_to_update;
  }

  /**
   * Get items queued for delete.
   *
   * @return QueueItemInterface[]
   */
  public function getItemsToDelete() {
    return $this->items_to_delete;
  }

  public function hasQueuedItems() {
    return $this->items_to_create || $this->items_to_update || $this->items_to_delete;
  }

  protected function flushQueue() {

    $this->controller->beforeFlushQueue($this);

    // Save queued items.
    foreach ($this->items_to_create as $item) {
      $this->controller->saveItem($item);
    }
    foreach ($this->items_to_update as $item) {
      $this->controller->saveItem($item);
    }

    $this->controller->whileFlushQueueBeforeDelete($this);

    // Delete queued items.
    foreach ($this->items_to_delete as $item) {
      $this->controller->deleteItem($item);
    }

  }

  /**
   * Get all items.
   *
   * @param bool $uncached
   * @return mixed[]
   */
  public function getEffectiveItems($uncached = FALSE) {
    $items = $this->controller->getStoredItems($uncached);

    $this->controller->adjustStoredItems($items);

    // Put in items to save or delete.
    foreach ($this->items_to_create as $item) {
      $items[$this->controller->getItemKey($item)] = $item;
    }
    foreach ($this->items_to_update as $item) {
      $items[$this->controller->getItemKey($item)] = $item;
    }
    foreach ($this->items_to_delete as $item) {
      unset($items[$this->controller->getItemKey($item)]);
    }
    return $items;
  }

  /**
   * @return \Drupal\dateseries\CRUDQueue\QueueControllerInterface
   */
  public function getController() {
    return $this->controller;
  }

}
