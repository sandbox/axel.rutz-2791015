<?php
/**
 * @file QueueControllerInterface.php
 */
namespace Drupal\dateseries\CRUDQueue;

/**
 * Interface QueueControllerInterface
 * @package Drupal\dateseries\CRUDQueue
 *
 * A queue is constructed with a controller that implements this interface
 * and gives the queue items some meaning.
 */
interface QueueControllerInterface {
  public function beforeFlushQueue(Queue $queue);

  public function whileFlushQueueBeforeDelete(Queue $queue);

  public function afterFlushQueue(Queue $queue);

  /**
   * @param mixed $item
   * @return boolean
   */
  public function checkItemType($item);

  public function isUnsavedItem($event);

  public function saveItem($item);

  public function deleteItem($item);

  /**
   * @param mixed $item
   * @return int|string
   */
  public function getItemKey($item);

    /**
   * @param boolean $uncached
   * @return mixed[]
   */
  public function getStoredItems($uncached);

  /**
   * @param mixed[] $items
   */
  public function adjustStoredItems(array &$items);

}
