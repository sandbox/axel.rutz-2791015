<?php
/**
 * @file SimpleSyncer.php
 */

namespace Drupal\dateseries\DatesUpdater;


use Drupal\dateseries\CRUDQueue\Queue;
use Drupal\dateseries\Model\Event;
use Drupal\dateseries\Tools\CompareFieldWrappers;


/**
 * Class DatesUpdaterSimple
 * @package Drupal\dateseries\Syncer
 *
 *  This simple updater ignores the current dates field simply and rebuilds the
 * dates field on every event date change.
 */
class DatesUpdaterSimple implements DatesUpdaterInterface {

  /**
   * Calculate changes to dates field.
   *
   * @param Queue $queue
   * @return \EntityStructureWrapper[]
   */
  public function getUpdatedSeriesDates(Queue $queue) {
    /** @var Event[] $events */
    $events = $queue->getEffectiveItems();
    // Sort by date.
    usort($events, function (Event $event1, Event $event2) {
      return CompareFieldWrappers::compareNormalized($event1->getFieldDate(), $event2->getFieldDate());
    });

    $date_field_items = array_filter(array_map(function (Event $event) {
      return $event->getFieldDate();
    }, $events));
    return $date_field_items;
  }

}
