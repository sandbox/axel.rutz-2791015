<?php
/**
 * @file SyncerInterface.php
 */
namespace Drupal\dateseries\DatesUpdater;

use Drupal\dateseries\CRUDQueue\Queue;

/**
 * Interface DatesUpdaterInterface
 * @package Drupal\dateseries\DatesUpdater
 */
interface DatesUpdaterInterface {
  /**
   * Sync changes to dates field.
   *
   * @param Queue $queue
   * @return
   */
  public function getUpdatedSeriesDates(Queue $queue);

}
