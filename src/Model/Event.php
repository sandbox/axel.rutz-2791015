<?php
/**
 * @file Event.php
 */

namespace Drupal\dateseries\Model;


use Drupal\dateseries\Controller\EntityControllerInterface;
use Drupal\dateseries\CRUDQueue\QueueItemInterface;
use Drupal\dateseries\Tools\DateTools;
use Drupal\dateseries\Tools\EntityTools;


class Event extends Base implements QueueItemInterface {
  /** @var Series|null */
  protected $series;

  public function __construct(EntityControllerInterface $entity_controller, \EntityDrupalWrapper $entity = NULL) {
    parent::__construct($entity_controller, $entity);
    /** @var \EntityDrupalWrapper $series_entity */
    $series_entity = $this->getFieldSeries();
    if ($series_entity && $series_entity->value()) {
      $this->series = new Series($entity_controller, $series_entity);
    }
  }

  /**
   * Get series.
   *
   * @return Series
   */
  public function getSeries() {
    return $this->series;
  }

  /**
   * Set series.
   *
   * @param Series|null $series
   */
  public function setSeries(Series $series = NULL) {
    $this->series = $series;
    $series_entity = $series ? $series->getEntity() : NULL;
    $this->getFieldSeries()->set($series_entity->value());
  }

  /**
   * Get series entity from our event entity.
   * @return mixed
   */
  protected function getFieldSeries() {
    return $this->entity->get($this->entity_controller->getEventFieldSeries());
  }

  /**
   * @return \EntityStructureWrapper
   */
  public function getFieldDate() {
    return $this->entity->get($this->entity_controller->getEventFieldDate());
  }

  /**
   * @param \EntityStructureWrapper $date_item
   */
  public function setFieldDate($date_item) {
    $this->getFieldDate()->set($date_item->value());
  }

  /**
   * Check if the entity has field: dates.
   *
   * @return bool
   */
  public function hasFieldDates() {
    return EntityTools::entityHasField($this->entity, $this->entity_controller->getEventFieldDates());
  }

  /**
   * @return \EntityListWrapper
   */
  public function getFieldDates() {
    return $this->entity->get($this->entity_controller->getEventFieldDates());
  }

  /**
   * Check if the entity has field: dates.
   *
   * @return bool
   */
  public function hasFieldAdd() {
    return EntityTools::entityHasField($this->entity, $this->entity_controller->getEventFieldAdd());
  }

  /**
   * @return \EntityListWrapper
   */
  public function getFieldAdd() {
    $value = $this->entity->get($this->entity_controller->getEventFieldAdd());
    DateTools::filterOutEmptyDates($value);
    return $value;
  }

  /**
   * Replicate for dates.
   *
   * @param \EntityStructureWrapper[] $date
   * @return Event[]
   */
  public function replicateForDates(array $dates) {
    return array_map(function ($date) {
      return $this->replicateForDate($date);
    }, $dates);
  }

  /**
   * Replicate for date.
   *
   * @param \EntityStructureWrapper $date
   * @return Event
   */
  public function replicateForDate(\EntityStructureWrapper $date) {
    /** @var Event $clone */
    $clone = $this->replicate();
    $clone->setFieldDate($date);
    return $clone;
  }

  /**
   * Replicate an Event.
   *
   * @return Event
   */
  public function replicate() {
    $replicated_entity_raw = replicate_clone_entity($this->entity->type(), $this->entity->value());
    unset($replicated_entity_raw->original);
    /** @var \EntityDrupalWrapper $replicated_entity_wrapped */
    $replicated_entity_wrapped = entity_metadata_wrapper($this->entity->type(), $replicated_entity_raw);
    $event = new Event($this->entity_controller, $replicated_entity_wrapped);
    return $event;
  }

  /**
   * Create a new series entity.
   *
   * @return Series
   */
  public function createSeries() {
    $entity_type = $this->entity_controller->getSeriesEntityType();
    $bundle = $this->entity_controller->getSeriesEntityBundle();
    $series_entity = EntityTools::createEntityWrapped($entity_type, $bundle);
    $series = new Series($this->entity_controller, $series_entity);
    return $series;
  }


}
