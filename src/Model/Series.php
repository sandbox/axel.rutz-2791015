<?php
/**
 * @file Series.php
 */

namespace Drupal\dateseries\Model;


use Drupal\dateseries\Tools\EntityTools;
use Drupal\dateseries\Tools\FixListWrappers;

class Series extends Base {

  /**
   * Check if the entity has field: dates.
   *
   * @return bool
   */
  public function hasFieldDates() {
    return EntityTools::entityHasField($this->entity, $this->entity_controller->getSeriesFieldDates());
  }

  /**
   * @return \EntityListWrapper
   */
  public function getFieldDates() {
    return $this->entity->get($this->entity_controller->getSeriesFieldDates());
  }

  /**
   * Set dates.
   *
   * @param \EntityListWrapper|\EntityStructureWrapper[] $dates
   */
  public function setFieldDates($dates) {
    FixListWrappers::setList($this->getFieldDates(), $dates);
  }

}
