<?php
/**
 * @file Base.php
 */

namespace Drupal\dateseries\Model;


use Drupal\dateseries\Controller\EntityControllerInterface;


abstract class Base {
  /** @var \EntityDrupalWrapper */
  protected $entity;
  /** @var EntityControllerInterface */
  protected $entity_controller;

  /**
   * Base constructor.
   *
   * @param EntityControllerInterface $entity_controller
   * @param \EntityDrupalWrapper|null $entity
   */
  public function __construct(EntityControllerInterface $entity_controller, \EntityDrupalWrapper $entity) {
    $this->entity_controller = $entity_controller;
    $this->entity = $entity;
  }

  /**
   * @return \EntityDrupalWrapper
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * @return EntityControllerInterface
   */
  public function getEntityController() {
    return $this->entity_controller;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->entity->getIdentifier();
  }

  public function saveUnprocessed() {
    $this->markEntityToPreventHookProcessing();
    $this->entity->save();
  }

  public function deleteUnprocessed() {
    $this->markEntityToPreventHookProcessing();
    $this->entity->delete();
  }

  protected function markEntityToPreventHookProcessing() {
    $this->entity->value()->dateseries_prevent_hook_processing = TRUE;
  }

  public function checkHookProcessingPrevented() {
    return !empty($this->entity->value()->dateseries_prevent_hook_processing);
  }

}
